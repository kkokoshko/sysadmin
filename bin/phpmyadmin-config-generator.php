#!/usr/bin/php
<?php
/**
 * phpmyadmin-config-generator - takes a list of hostnames from a file and generates phpmyadmin config files for each in ./phpma-conf.d/ directory
 *
 * ../.env file should contain paths to SSL certificates:
 *
 * SSL_CA_CERT
 * SSL_CLIENT_CERT
 * SSL_CLIENT_KEY
 *
 */

$envfile = "../.env";
$outdir = "./phpma-conf.d/";
$usage = $argv[0] . " <hosts_file_path>\n";

if ( count($argv) <= 1 ) {
    echo "$usage";
    exit (99);
}

$hostsfile = $argv[1];

if ( ! file_exists($envfile) ) {
    echo "Missing ../.env file, please create\n$usage";
    exit (1);
}

if ( ! file_exists($hostsfile) ) {
    echo "Missing hosts file: $hostsfile\n$usage";
    exit (2);
}

if (!mkdir($outdir, 0755) && !is_dir($outdir)) {
    echo "Directory $outdir does not exist and could not be created";
}

$env_contents = file_get_contents($envfile);
$hosts = preg_split("/[\s]/",file_get_contents($hostsfile));

// print_r($hosts);

$enva = [];

$tempa = preg_split("/\s/", $env_contents);
// print_r($tempa);

foreach ( $tempa as $templ ) {
    if ( preg_match("/(.+)=(.+)/", $templ,$matches)) {
        $enva[$matches[1]] = $matches[2];
        // echo "match: " . $matches[1] . "=" . $matches[2] . "\n";
    } else {
        // echo "no match: $templ\n";
    }
}

$sslca = $enva['SSL_CA_CERT'];
$sslcert = $enva['SSL_CLIENT_CERT'];
$sslkey = $enva['SSL_CLIENT_KEY'];

foreach ( $hosts as $host ) {

    $name = preg_split("/\./",$host);
    $name = $name[0];

    // echo "NAME: $name\n";

    if ( empty($name) ) {
        continue;
    }

    $record = "<?php
/* $name mysql config */
\$cfg['Servers'][\$i]['verbose'] = '$name';
\$cfg['Servers'][\$i]['host'] = '$host';
\$cfg['Servers'][\$i]['port'] = '3306';
\$cfg['Servers'][\$i]['socket'] = '';
\$cfg['Servers'][\$i]['connect_type'] = 'tcp';
\$cfg['Servers'][\$i]['extension'] = 'mysqli';
\$cfg['Servers'][\$i]['auth_type'] = 'cookie';
\$cfg['Servers'][\$i]['AllowNoPassword'] = false;
\$cfg['Servers'][\$i]['ssl'] = true;
\$cfg['Servers'][\$i]['ssl_verify'] = false;
\$cfg['Servers'][\$i]['ssl_ciphers'] = 'DHE-RSA-AES256-SHA';
\$cfg['Servers'][\$i]['ssl_ca'] = '$sslca';
\$cfg['Servers'][\$i]['ssl_cert'] = '$sslcert';
\$cfg['Servers'][\$i]['ssl_key'] = '$sslkey';
\$i++;
";
    file_put_contents("$outdir/$host.php", $record);
}
